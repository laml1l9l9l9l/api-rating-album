const fs = require('fs')
const Album = require('../models/album.model')

module.exports.index = async (req, res) => {
  const page = req.query.page || 1;
  const limit = req.query.limit || 1;
  try {
    const albums = await Album.find()
      .limit(limit * 1)
      .skip((page - 1) * limit);
    res.json(albums);
  } catch (err) {
    res.status(400).json({
      error: err
    })
  }
}

module.exports.specific = async (req, res) => {
  try {
    const album = await Album.findById(req.params.id);
    res.json(album);
  } catch (err) {
    res.status(400).json({
      error: err
    })
  }
}

module.exports.store = async (req, res) => {
  // const processedFile = req.file || {};
  // let orgName = processedFile.originalname || '';
  // orgName = orgName.trim().replace(/ /g, "-");
  // const fullPathInServer = processedFile.path;
  // const newFullPath = `${fullPathInServer}-${orgName}`;
  // fs.renameSync(fullPathInServer, newFullPath);
  const album = new Album({
    title: req.body.title,
    // urlImage: newFullPath,
    urlImage: "",
    dateRelease: req.body.dateRelease,
    songs: req.body.songs,
    comments: req.body.comments,
    rating: req.body.rating
  });
  console.log(album);
  return "ok";
  try {
    const storeAlbum = await album.save();
    res.json(storeAlbum);
  } catch (err) {
    res.status(400).json({
      error: err
    })
  }
}

module.exports.update = async (req, res) => {
  const albumUpdate = {};
  if (req.body.title) albumUpdate.title = req.body.title;
  if (req.file) {
    const processedFile = req.file || {};
    let orgName = processedFile.originalname || '';
    orgName = orgName.trim().replace(/ /g, "-");
    const fullPathInServer = processedFile.path;
    const newFullPath = `${fullPathInServer}-${orgName}`;
    fs.renameSync(fullPathInServer, newFullPath);
    albumUpdate.urlImage = newFullPath;
  };
  if (req.body.dateRelease) albumUpdate.dateRelease = req.body.dateRelease;

  try {
    const oldAlbum = await Album.findById(req.params.id);
    fs.unlinkSync(oldAlbum.urlImage);
    const album = await Album.updateOne(
      {
        _id: req.params.id
      },
      {
        $set: albumUpdate
      }
    );
    res.json(album);
  } catch (err) {
    res.status(400).json({
      error: err
    });
  }
}

module.exports.delete = async (req, res) => {
  try {
    const oldAlbum = await Album.findById(req.params.id);
    fs.unlinkSync(oldAlbum.urlImage);
    const removeAlbum = await Album.remove({
      _id: req.params.id
    });
    res.json(removeAlbum);
  } catch (err) {
    res.status(400).json({
      error: err
    })
  }
}