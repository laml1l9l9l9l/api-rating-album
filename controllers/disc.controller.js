const Disc = require('../models/disc.model')

module.exports.index = async (req, res) => {
  const page = req.query.page || 1;
  const limit = req.query.limit || 1;
  try {
    const discs = await Disc.find()
      .limit(limit * 1)
      .skip((page - 1) * limit);
    res.json(discs);
  } catch (err) {
    res.status(400).json({
      error: err
    });
  }
}

module.exports.specificAlbum = async (req, res) => {
  try {
    const disc = await Disc.find({
      idAlbum: req.params.id
    });
    res.json(disc);
  } catch (err) {
    res.status(400).json({
      error: err
    });
  }
}

module.exports.store = async (req, res) => {
  const disc = new Disc({
    idAlbum: req.body.idAlbum,
    idSong: req.body.idSong
  });

  try {
    const storeDisc = await disc.save();
    res.json(storeDisc);
  } catch (err) {
    res.status(400).json({
      error: err
    });
  }
}

module.exports.updateSongAlbum = async (req, res) => {
  const idDisc = req.params.id;

  try {
    const disc = await Disc.findById(idDisc);
    const discExist = await Disc.find({
      idAlbum: disc.idAlbum,
      idSong: req.body.idSong
    });
    if (discExist.length) return res.status(400).json({ error: 'Song exist'});
    const updateDisc = await Disc.updateOne(
      {
        _id: idDisc
      },
      {
        $set: {
          idSong: req.body.idSong
        }
      }
    );
    res.json(updateDisc);
  } catch (err) {
    res.status(400).json({
      error: err
    });
  }
}

module.exports.delete = async (req, res) => {
  try {
    const removeDisc = await Disc.remove({
      _id: req.params.id
    });
    res.json(removeDisc);
  } catch (err) {
    res.status(400).json({
      error: err
    });
  }
}