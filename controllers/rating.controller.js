const Rating = require('../models/rating.model')

module.exports.index = async (req, res) => {
  const page = req.query.page || 1;
  const limit = req.query.limit || 1;
  try {
    const allRating = await Rating.find()
      .limit(limit * 1)
      .skip((page - 1) * limit);
    return res.json(allRating);
  } catch (err) {
    return res.status(400).json({
      error: err
    });
  }
}

module.exports.specific = async (req, res) => {
  try {
    const rating = await Rating.findById(req.params.id);
    res.json(rating);
  } catch (err) {
    res.status(400).json({
      error: err
    });
  }
}

module.exports.store = async (req, res) => {
  const { idUser, idAlbum } = req.body;
  const rating = new Rating({
    idUser: req.body.idUser,
    idAlbum: req.body.idAlbum
  });
  const handlingUserRated = async (idUser, idAlbum) => {
    const ratingExist = await Rating.find({
      idUser: idUser,
      idAlbum: idAlbum
    });
    if(ratingExist.length > 0) return false;
    return true;
  };
  const userRated = await handlingUserRated(idUser, idAlbum);
  if(!userRated) res.status(400).json({ error: 'You rated album' });
  try {
    const storeRating = await rating.save();
    res.json(storeRating);
  } catch (err) {
    res.status(400).json({
      error: err
    });
  }
}

module.exports.delete = async (req, res) => {
  try {
    const removeRating = await Rating.remove({
      _id: req.params.id
    });
    res.json(removeRating);
  } catch (err) {
    res.status(400).json({
      error: err
    });
  }
}