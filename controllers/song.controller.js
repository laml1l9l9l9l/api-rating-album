const Song = require('../models/song.model')

module.exports.index = async (req, res) => {
  const page = req.query.page || 1;
  const limit = req.query.limit || 1;
  try {
    const songs = await Song.find()
      .limit(limit * 1)
      .skip((page - 1) * limit);
    res.json(songs);
  } catch (err) {
    res.status(400).json({
      error: err
    });
  }
}

module.exports.specific = async (req, res) => {
  try {
    const song = await Song.findById(req.params.id);
    res.json(song);
  } catch (err) {
    res.status(400).json({
      error: err
    });
  }
}

module.exports.store = async (req, res) => {
  const song = new Song({
    name: req.body.name
  });
  try {
    const storeSong = await song.save();
    res.json(storeSong);
  } catch (err) {
    res.status(400).json({
      error: err
    });
  }
}

module.exports.update = async (req, res) => {
  const songUpdate = {};
  if (req.body.name) songUpdate.name = req.body.name;

  try {
    const song = await Song.updateOne(
      {
        _id: req.params.id
      },
      {
        $set: songUpdate
      }
    );
    res.json(song);
  } catch (err) {
    res.status(400).json({
      error: err
    });
  }
}

module.exports.delete = async (req, res) => {
  try {
    const removeSong = await Song.remove({
      _id: req.params.id
    });
    res.json(removeSong);
  } catch (err) {
    res.status(400).json({
      error: err
    });
  }
}