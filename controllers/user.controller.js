const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const User = require('../models/user.model')
const AuthValidation = require('./validations/auth.validation')

module.exports.register = async (req, res) => {
  const { error } = AuthValidation.register(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const emailExist = await User.findOne({
    email: req.body.email
  });
  if (emailExist) return res.status(400).send('Email already exists');

  const salt = await bcrypt.genSalt(10);
  const hashPassword = await bcrypt.hash(req.body.password, salt);

  const user = new User({
    email: req.body.email,
    password: hashPassword,
    firstName: req.body.firstName,
    lastName: req.body.lastName
  });
  try {
    const registerUser = await user.save();
    const token = jwt.sign(
      {
        _id: user._id
      },
      process.env.TOKEN_SECRET,
      {
        expiresIn: '2h'
      }
    );
    res.header('auth-token', token).send(token);
  } catch (err) {
    res.status(400).send(err);
  }
}

module.exports.login = async (req, res) => {
  const { error } = AuthValidation.login(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const user = await User.findOne({
    email: req.body.email
  });
  if (!user) res.status(400).send('Email not found');

  const validPassword = bcrypt.compare(req.body.password, user.password);
  if (!validPassword) return res.status(400).send('Invalid password');

  const token = jwt.sign(
    {
      _id: user._id
    },
    process.env.TOKEN_SECRET,
    {
      expiresIn: '2h'
    }
  );
  res.header('auth-token', token).send(token);
}