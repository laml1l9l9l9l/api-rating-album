const Joi = require('@hapi/joi')

module.exports.register = data => {
  const schema = Joi.object({
    email: Joi.string()
      .min(6)
      .required()
      .email(),
    password: Joi.string()
      .min(6)
      .required(),
    firstName: Joi.string()
      .min(6)
      .required(),
    lastName: Joi.string()
      .min(6)
      .required()
  });

  return schema.validate(data);
}

module.exports.login = data => {
  const schema = Joi.object({
    email: Joi.string()
      .min(6)
      .required()
      .email(),
    password: Joi.string()
      .min(6)
      .required()
  })

  return schema.validate(data);
}