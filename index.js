require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const bodyParser = require('body-parser')
const adminRoute = require('./routes/admin.route')
const userRoute = require('./routes/user.route')
const albumRoute = require('./routes/album.route')
const songRoute = require('./routes/song.route')
const discRoute = require('./routes/disc.route')
const ratingRoute = require('./routes/rating.route')
const authMiddleware = require('./middleware/auth.middleware')

const app = express()
const port = process.env.PORT || 5000

app.use(cors())
app.use(bodyParser.json())

mongoose.connect(process.env.DB_CONNECTION)
mongoose.set('useNewUrlParser', true)
mongoose.set('useFindAndModify', false)
mongoose.set('useCreateIndex', true)

app.use('/api/admin', adminRoute)
app.use('/api/user', userRoute)
app.use(authMiddleware)
app.use('/api/album', albumRoute)
app.use('/api/song', songRoute)
app.use('/api/disc', discRoute)
app.use('/api/rating', ratingRoute)

app.listen(
  port,
  () => console.log(`App listening at http://localhost:${port}`)
)