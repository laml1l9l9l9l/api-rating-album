const mongoose = require('mongoose')

const adminSchema = mongoose.Schema({
  email: {
    type: String,
    require: true
  },
  password: {
    type: String,
    require: true
  },
  firstName: {
    type: String,
    require: true
  },
  lastName: {
    type: String,
    require: true
  },
  isAdmin: Boolean
}, {
  timestamps: true
})

const Admin = mongoose.model('Admin', adminSchema, 'admins')

module.exports = Admin