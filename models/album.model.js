const mongoose = require('mongoose')
const User = require('./user.model')

const albumSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  urlImage: {
    type: String,
    required: true,
    unique: true
  },
  dateRelease: {
    type: String,
    required: true
  },
  songs: [String],
  comments: [{
    type: new mongoose.Schema({
      idUser: {
        type: mongoose.ObjectId,
        ref: User
      },
      content: String,
      isReply: Boolean,
      commentsReply: [{
        type: new mongoose.Schema({
          idUser: {
            type: mongoose.ObjectId,
            ref: User
          },
          content: String,
        }, {
          timestamps: true
        })
      }]
    }, {
      timestamps: true
    })
  }],
  rating: [{
    type: mongoose.ObjectId,
    ref: User
  }]
}, {
  timestamps: true
})

const Album = mongoose.model('Album', albumSchema, 'albums')

module.exports = Album