const mongoose = require('mongoose')
const Album = require('./album.model')
const Song = require('./song.model')

const discSchema = mongoose.Schema({
  idAlbum: {
    type: mongoose.ObjectId,
    ref: Album,
    require: true
  },
  idSong: {
    type: mongoose.ObjectId,
    ref: Song,
    require: true
  }
}, {
  timestamps: true
})

const Disc = mongoose.model('Disc', discSchema, 'discs')

module.exports = Disc