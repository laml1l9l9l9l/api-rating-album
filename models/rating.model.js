const mongoose = require('mongoose')
const Album = require('./album.model')
const User = require('./user.model')

const ratingSchema = mongoose.Schema({
  idUser: {
    type: mongoose.ObjectId,
    ref: User,
    require: true
  },
  idAlbum: {
    type: mongoose.ObjectId,
    ref: Album,
    require: true
  }
}, {
  timestamps: true
})

const Rating = mongoose.model('Rating', ratingSchema, 'rating')

module.exports = Rating