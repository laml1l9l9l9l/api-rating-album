const mongoose = require('mongoose')

const songSchema = new mongoose.Schema({
  name: {
    type: String,
    require: true
  }
}, {
  timestamps: true
})

const Song = mongoose.model('Song', songSchema, 'songs')

module.exports = Song