const express = require('express')
const multer = require('multer')
const imageUpload = multer({ dest: 'public/images/' })
const controller = require('../controllers/album.controller')

const router = express.Router()

router.get('/', controller.index)
router.get('/:id', controller.specific)
router.post('/', imageUpload.single('image'), controller.store)
router.patch('/:id', imageUpload.single('image'), controller.update)
router.delete('/:id', controller.delete)

module.exports = router