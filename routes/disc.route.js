const express = require('express')
const controller = require('../controllers/disc.controller')
const router = express.Router()

router.get('/', controller.index)
router.get('/:id', controller.specificAlbum)
router.post('/', controller.store)
router.patch('/:id', controller.updateSongAlbum)
router.delete('/:id', controller.delete)

module.exports = router