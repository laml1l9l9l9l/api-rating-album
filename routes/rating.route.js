const express = require('express')
const controller = require('../controllers/rating.controller')
const router = express.Router()

router.get('/', controller.index)
router.get('/:id', controller.specific)
router.post('/', controller.store)
router.delete('/:id', controller.delete)

module.exports = router