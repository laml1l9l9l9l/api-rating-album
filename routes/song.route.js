const express = require('express')
const controller = require('../controllers/song.controller')

const router = express.Router()

router.get('/', controller.index)
router.get('/:id', controller.specific)
router.post('/', controller.store)
router.patch('/:id', controller.update)
router.delete('/:id', controller.delete)

module.exports = router